#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>// required for memset
#include <time.h>// for benchmarking

float startTime;
float endTime;
float timeElapsed;

uint16_t *image;

void sumframes(int height, int width, int detectors, int frames, uint16_t image[frames][detectors][height][width]);


int main(int argc, char *argv[])
{
    char *flags = ":lvhH:W:";
    int option;
    bool BigEndianInputFile = true;
    int BlocksWide = 32;
    int BlocksHigh = 32;
    
       // put ':' at the starting of the string so compiler can distinguish between '?' and ':'
    while((option = getopt(argc, argv, flags)) != -1)
    { //get option from the getopt() method
        switch(option)
        {
         //l used for setting endianness of the input file to little endian.
        case 'l':
            printf("Given Option: %c\n", option);
            BigEndianInputFile = false;
            break;
        case 'h':
            printf("Help\n");
            exit(0);
            break;
        case 'H': 
        {    
            //Set BlocksHigh (the number of blocks in the y direction)
            // set a long for the return value of strtol
            long l = -1;
            // use strtol to turn optarg string into a long
            l=strtol(optarg, NULL, 0);
            if (l > 0)
            {
                // cast long l to an int and assign to BlocksHigh
                BlocksHigh = (int) l;
            }
            else
            {
                printf("Invalid -H option: '%s'\n", optarg);
                exit(0);
            }
            break;
        }
        case 'W':
        {        
            // Set BlocksWide (the number of blocks in the x direction)
            // set a long for the return value of strtol
            long l = -1;
            // use strtol to turn optarg string into a long
            l=strtol(optarg, NULL, 0);
            if (l > 0)
            {
                // Cast it to an int
                BlocksWide = (int) l;
            }
            else
            {
                printf("Invalid -H option: '%s'\n", optarg);
                exit(0);
            }
            break;
        }        case ':':
            printf("Option needs a value\n");
            break;
        case '?': //used for some unknown options
            printf("unknown option: %c\n", optopt);
            break;
        }
    }
    //    for(; optind < argc; optind++){ //when some extra arguments are passed
    //       printf("Given extra arguments: %s\n", argv[optind]);

    // optind is the index of the next argv to be processed by getopt
    // check for one or two additional args, infile and outfile

    char *infile;
    char *outfile;
    if (argc == optind + 1)
    {
        infile = argv[optind];
        outfile = "output.dat";
        printf("Using %s as infile\n", argv[optind]);
        printf("No outfile giving, using %s\n", outfile);
    }    
    else if (argc == optind + 2)
    {
        infile = argv[optind];
        outfile = argv[optind + 1];
        printf("Using %s as infile\n", argv[optind]);
        printf("Using %s as outfile\n", argv[optind +1]);
    }
    else
    {
        fprintf(stderr, "Error, no file specified or improperly formatted arguments\n");
        printf("Usage: [flags] infile [outfile]\n");
        printf("Use -h for more information\n");
        return 3;
    }


        // Open input file
    printf("Opening file %s\n", infile);
    FILE *inptr = fopen(infile, "r");
    if (inptr == NULL)
    {
        fprintf(stderr, "Could not open %s.\n", infile);
        return 4;
    }
    else
    {
        printf("Opened file %s\n", infile);
    }
    
        // Open output file
    FILE *outptr = fopen(outfile, "w");
    if (outptr == NULL)
    {
        fclose(inptr);
        fprintf(stderr, "Could not create %s.\n", outfile);
        return 5;
    }


    // initialise an array of characters for the datestring
    // needs 8 characters + 1 extra for null terminator
    // seek to the address of the date
    // assign it to datestring using fgets (fgets handles null terminator)
    // could also use fread fread(&datestring, sizeof(char), 9, inptr);
    // since they are already followed by null bytes in the im file
    char datestring[9];
    fseek(inptr, 0x5c, SEEK_SET);
    fgets(datestring, 9, inptr);

    char timestring[6];
    fseek(inptr, 0x6c, SEEK_SET);
    fgets(timestring, 6, inptr);
    printf(".im file created on %s at %s\n", datestring, timestring);
    
    int height = 256;
    if (height % BlocksHigh != 0)
    {
        printf("Height of image not divisible by chosen number of blocks\n");
        printf("Height of image %ipx, number of blocks %i (remainder %i)\n", height, BlocksHigh, height % BlocksHigh);
        exit(0);
    }
    int BlockHeight = height / BlocksHigh;
    
    int width = 256;
    if (width % BlocksWide != 0)
    {
        printf("Width of image not divisble by chosen number of blocks\n");
        printf("Width of image %i, number of blocks %i (remainder %i)\n", width, BlocksWide, width % BlocksWide);
        exit(0);
    }
    int BlockWidth = width / BlocksWide; 
    
    uint8_t frames;
    fseek(inptr, 0x93, SEEK_SET);
    fread(&frames, sizeof(uint8_t), 1, inptr);
    printf("Frames: %i\n", frames);

    uint8_t detectors;
    fseek(inptr, 0x19b, SEEK_SET);
    fread(&detectors, sizeof(uint8_t), 1, inptr);
    printf("Detectors %i\n", detectors);

    //go to start of first detector metadata
    fseek(inptr, 0x1cc, SEEK_SET);
    
    // create an array of doubles for the mass tunings
    double masses[detectors];
    
    // create an array of strings for the mass names
    char detnames[detectors][0x40];
    
    // create a byte array into which we will read the double
    // this is necessary to correctly read big endian doubles
    // on systems with little endian native doubles
    uint8_t CurrentDouble[sizeof(double)];

    // Loop through the number of detectors to get radius and ion name
    for (int i = 0; i < detectors; i++)
    {
        printf("Detector %i, ", i + 1);

        // read the detector mass into an array of bytes called CurrentDouble
        fread(CurrentDouble, sizeof(uint8_t), sizeof(double), inptr);
        
        // Copy those bytes into a uint64_t, most significant first
        // (assumes the .im file is little endian)
        uint64_t CurrentDoubleAsLongLong;
        CurrentDoubleAsLongLong = (uint64_t)CurrentDouble[7] << 0 | (uint64_t)CurrentDouble[6] << 8 | (uint64_t)CurrentDouble[5] << 16 | (uint64_t)CurrentDouble[4] << 24 | (uint64_t)CurrentDouble[3] << 32 | (uint64_t)CurrentDouble[2] << 40 | (uint64_t)CurrentDouble[1] << 48 | (uint64_t)CurrentDouble[0] << 56;
        

        double * tmp_ptr;
        tmp_ptr = (double *)&CurrentDoubleAsLongLong;
        masses[i] = *tmp_ptr;
        
        // This next line is equivalent of the two commented out lines above
        // It casts the address of CurrentDoubleAsLong (which is uint64_t) to a pointer to a double
        // Then dereferences it so it is read as a double and assigned to masses[i] as a double
        //masses[i] = *(double *)&CurrentDoubleAsLongLong;

        
        printf("Radius %lf, ", masses[i]);
        // if the mass is 0, set the name to SE
        if (masses[i] == 0)
        {
            // could also use strcpy(detnames[i], "SE");
            detnames[i][0] = 'S';
            detnames[i][1] = 'E';
            detnames[i][2] = '\0';
            printf("%s \n", detnames[i]);
        }
        // otherwise set the name to whatever is stored in the file
        else
        {
        fseek(inptr, 0x31, SEEK_CUR);
        fread(&detnames[i], 0x40, 1, inptr);
        printf("%s\n", detnames[i]);
        fseek(inptr, 0x47, SEEK_CUR);
        }
    }

    // go to start of sims data
    fseek(inptr, 0x6740, SEEK_SET);
    
    // allocate memory for sims data
    int imagesize = height * width * sizeof(uint16_t);
    // create a pointer to an array of pixels that represents all 6 detectors
    // call it frames times - effectively creating an array of frames
    uint16_t(*image)[detectors][height][width] = calloc(imagesize * detectors, frames);
    if (image == NULL)
    {
        fprintf(stderr, "Not enough memory to store card data.\n");
        fclose(inptr);
        return 4;
    }
    
    printf("imagesize is %i\n", imagesize);
    
    // image data stored as shorts (uint16_t) arranged as pixels, then detectors then frames
    // read shorts one byte at a time, swap the bytes, then assign them to the image array
    uint8_t CurrentShort[2];
    for (int f = 0; f < frames; f++)
    {
        for (int d = 0; d < detectors; d++)
        {
            for (int h = 0; h < height; h++)
            {
                for (int w = 0; w < width; w++)
                {
                    fread(CurrentShort, sizeof(uint8_t), 2, inptr);
                    image[f][d][h][w] = CurrentShort[1] << 0 | CurrentShort[0] << 8;
                }
            }
        }
    }
    
    // sum all the frames
    printf("Summing frames\n");
    startTime = (float)clock()/CLOCKS_PER_SEC;

    sumframes(width, height, (int) detectors, (int) frames, image);
    
    endTime = (float)clock()/CLOCKS_PER_SEC;
    timeElapsed = endTime - startTime;

    printf(" Time taken %fms.\n", timeElapsed);

    
    //fwrite(image, imagesize, frames * detectors, outptr);
    
    
    // write detector names to file
    printf("Writing to file\n");
    startTime = (float)clock()/CLOCKS_PER_SEC;

    for (int d = 0; d < detectors; d++)
    {
        fprintf(outptr,"%s, ",detnames[d]);
        if (d == detectors - 1)
        {
            fprintf(outptr,"\n");
        }
    }
    
    
    // write sum of blocks to file
    uint64_t currentblock;
    // iterate over rows of blocks
    for (int l = 0; l < BlocksHigh; l++)
    {
        // iterate over blocks within the rows
        for (int k = 0; k < BlocksWide; k++)
        {
            
            // iterate over the detectors
            for (int d = 0; d < detectors; d++)
            {            
                // set sum of pixels in the currentblock to 0
                currentblock = 0;
                
                // iterate over rows and columns within block
                for (int j = 0; j < BlockHeight; j++)
                {
                    for (int i = 0; i < BlockWidth; i++)
                    {
                        currentblock += image[0][d][j + (BlockHeight * l)][i + (BlockWidth * k)];
                    }
                }
                fprintf(outptr,"%li, ",currentblock);
                
                if (d == detectors - 1)
                {
                    fprintf(outptr,"\n");
                }
            }
        }
    }

    endTime = (float)clock()/CLOCKS_PER_SEC;
    timeElapsed = endTime - startTime;
    printf("File written succesfully. Time taken %fms.\n", timeElapsed);
    
    // free memory
    free(image);
    // close files
    fclose(inptr);
    fclose(outptr);

}


void sumframes(int height, int width, int detectors, int frames, uint16_t image[frames][detectors][height][width])
{
    // This function reads all the pixels, and flips the endianness if necessary
    // By calling "ThisShort" function on each pixel.
    // Then it sums the frames (if more than one frame) into a buffer
    // And overwrites frame1 with the data held in the buffer
    
    
    //create a new array for the summed frames
    uint32_t buffer[detectors][height][width];
    
    //initialise it to zero
    //memset( buffer, 0, detectors*height*width*sizeof(uint16_t) );
    //this is commented out because each pixel can be initialised to 
    //zero before summing accross frames. Since we are looping through
    //the array anyway, it is just as fast.
    
//     printf("first pixel is %i\n", ThisShort(image[0][0][0][0]));
//     printf("second pixel is %i\n", ThisShort(image[0][0][0][1]));

    //loop over the frames, summing each pixel into buffer
    for (int l = 0; l < width; l++)
    {
        for (int k = 0; k < height; k++)
        {
            for (int j = 0; j < detectors; j++)
            {
                buffer[j][k][l] = 0;
                for (int i = 0; i < frames; i++) 
                {
                    buffer[j][k][l] += (image[i][j][k][l]);
                }
            }
        }
    }    
    
    //Overwrite the first frame with the buffer
    for (int l = 0; l < width; l++)
    {
        for (int k = 0; k < height; k++)
        {
            for (int j = 0; j < detectors; j++)
            {
                image[0][j][k][l] = buffer[j][k][l];
            }
        }
    }
        
    if (image[0][0][0][0] != buffer[0][0][0])
    {
        printf("Error summing frames\n");
    }
    else
    {
        printf("Frames summed succesfully.");
    }

    
}

